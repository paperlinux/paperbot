module.exports = {
	name: 'interactionCreate',
	execute(client, interaction){
		if(!interaction.isCommand()) return;
		try{
			client.commands.get(interaction.commandName).execute(interaction)
		}
		catch(e){
			console.log('[ERROR] ' + e)
		}
	}
}
