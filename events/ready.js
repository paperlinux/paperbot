require('dotenv').config()

module.exports = {
	name: "ready",
	async execute(client){
		console.log("Logged in as " + client.user.tag)
		// Register slash commands
		const devGuild = await client.guilds.cache.get(process.env.DEVGUILDID)
		devGuild.commands.set(client.commands.map(cmd => cmd))
	}
}
