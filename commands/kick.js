// Kick command

module.exports = {
	name: 'kick',
	description: 'Expulse un utilisateur du serveur',
	usage: '<@mention> <raison>',
	category: 'modération',
	args: true,
	guildOnly: true,
	adminOnly: true,
	options: [
		{
			name: 'utilisateur',
			description: 'L\'utilisateur à expulser',
			type: 'USER',
			required: true
		},
		{
			name: 'raison',
			description: 'La raison de l\'expulsion',
			type: 'STRING',
			required: true
		}
	],
	async execute(interaction) {
		// First, we get the user to kick
		const user = interaction.options.get('utilisateur').user;
		// Then, we get the reason
		const reason = interaction.options.get('raison').value;
		// Then, we kick the user if author is admin
		// If author is not admin, we send a message
		if (interaction.member.permissions.has('ADMINISTRATOR')) {
			// Get the member to kick
			const member = await interaction.member.guild.members.fetch(user.id);
			// Kick the member
			member.kick(reason).then(() => {
				// Send a message to the channel
				interaction.reply(`${user.username} a été expulsé du serveur par ${interaction.member.user.username} pour la raison suivante : ${reason}`);
			}).catch(() => {
				// Send a message to the channel
				interaction.reply(`Je n'ai pas pu expulser ${user.username}`);
			})
		} else {
			// Send a message to the channel
			interaction.reply(`Vous n'avez pas la permission d'expulser ${user.username}`);
		}
	}
};
