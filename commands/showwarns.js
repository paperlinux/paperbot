// showwarns command

//Include fs module
const fs = require('fs');
const Discord = require('discord.js');

module.exports = {
	name: 'showwarns',
	description: 'Montre les avertissements d\'un utilisateur',
	options: [
		{
			name: 'user',
			description: 'L\'utilisateur dont on veut voir les avertissements',
			required: true,
			type: 'USER'
		}
	],

	execute(interaction) {
		// Load DB of ./warns.json
		const warns = JSON.parse(fs.readFileSync('./warns.json', 'utf8'));

		//Check if the user has warns
		if (!warns[interaction.options.get('user').user.id]) {
			interaction.reply(`${interaction.options.get('user').user.username} n'a pas d'avertissement.`);
			return;
		}
		//If the user has warns, send them as an embed
		const embed = new Discord.MessageEmbed()
			.setTitle(`Avertissements de ${interaction.options.get('user').user.username}`)
			.setColor(0x00AE86)

		//For each warn, add a field to the embed
		//The field will be (title => server, content => reason, when => date (with format DD/MM/YY), who => moderator)
		warns[interaction.options.get('user').user.id].forEach(warn => {
			embed.addField(`${warn.guild.name}`, `Raison => ${warn.raison.value} \n Date => ${new Date(warn.date).toLocaleDateString()} \n Staff => ${warn.moderator.displayName}`);
		});

		interaction.reply({embeds: [embed]});
	}
};

