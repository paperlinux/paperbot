// Include Discord module
const Discord = require('discord.js');

module.exports = {
	name: 'ban',
	description: 'Bannir un utilisateur',
	usage: '<utilisateur>',
	args: true,
	options: [
		{
			name: 'user',
			description: 'L\'utilisateur à bannir',
			type: 'USER',
			required: true
		},
		{
			name: 'raison',
			description: 'La raison du ban',
			type: 'STRING',
			required: true
		}
	],
	guildOnly: true,
	async execute(interaction) {
		const user = interaction.options.get('user');
		if (!user) {
			return interaction.reply('Vous devez mentionner un utilisateur');
		}
		const member = await interaction.member.guild.members.fetch(user);
		if (!member) {
			return interaction.reply('Cet utilisateur n\'est pas sur le serveur');
		}
		if (!interaction.member.permissions.has('BAN_MEMBERS')) {
			return interaction.reply('Vous ne pouvez pas bannir cet utilisateur');
		}
		if (member.bannable) {
			member.ban({ reason: interaction.options.get('raison') });
			// Generate a log embed
			const embed = new Discord.MessageEmbed()
				.setColor('#ff0000')
				.setTitle('Ban')
				.setDescription(`${user.tag} a été banni du serveur par ${message.author.tag}`)
				.addField('Raison', interaction.options.get('raison'))
				.setTimestamp();
			// Send the log embed
			interaction.channel.send(embed);
			interaction.reply();
		}
		return interaction.reply('Je ne peux pas bannir cet utilisateur');
	},
};
